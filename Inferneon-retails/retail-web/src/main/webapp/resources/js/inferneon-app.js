'use strict';


var inferneonApp = angular.module('inferneonApp', ['ui.router','ngGrid','ui.bootstrap','editableTableWidgets', 'frontendServices', 'commonServices','spring-security-csrf-token-interceptor', 'ngFileUpload'])

inferneonApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/');
	$stateProvider
    .state('home', {
    	    url:'/',
			templateUrl : '/resources/pages/retail/dashBoard.html',
			controller  : 'InferneonCtrl'
		})
}]);
inferneonApp.factory("MessageBus", function($rootScope) {
	return {
		broadcast : function(event, data) {
			$rootScope.$broadcast(event, data);
		}
	};
});
/**
 * Used to retrieve system configuration
 */
inferneonApp.controller('InferneonCtrl', ['$scope' ,'$http','$location','Upload', '$rootScope','MessageService','$timeout', '$uibModal','RetailService','MessageBus','UserService',
        function ($scope, $http,$location,Upload, $rootScope, MessageService, $timeout, $uibModal,RetailService,MessageBus,UserService) {
		$scope.vm.data = [];
		$scope.vm.appReady = true;
             UserService.getUserInfo()
                 .then(function (userInfo) {
                     $scope.vm.userName = userInfo.userName;
                 },
                 function (errorMessage) {
                 	MessageService.showErrorMessage(errorMessage);
                 });
		$http.get("json/treeView.json").then(function(response){
			$scope.jsonData = response.data;
			$('#tree').treeview({data: $scope.jsonData,
				expandIcon: 'glyphicon glyphicon-folder-close',
				collapseIcon: 'glyphicon glyphicon-folder-open',
				nodeIcon: 'glyphicon glyphicon-file',
				selectedIcon: 'glyphicon glyphicon-selected',
				onNodeSelected: function(event, node) {
					console.log(node);
				}
			})
		})
		
		$http.get("json/score_bord.json").then(function(response){
			$scope.scoreboardData = response.data.data;
			$scope.finalScoreBoardData = [];
			_.each($scope.scoreboardData,function(data){
				_.each(data.rowData,function(rowsData){
					$scope.finalScoreBoardData.push({rowName:data.rowName,subrowName:rowsData.rowName})
				_.each(rowsData.rowData,function(columnValues){
					$scope.finalScoreBoardData.push({columnValue :columnValues.columnValue})
				})
				})
			})
		})
		$http.get("json/scoreboard_discounts.json").then(function(response){
			$scope.scoreboardDataDiscounts = response.data.dataForDiscounts;
			$scope.scoreboardDataSPSF = response.data.dataForSPSF;
			$scope.dataForSOH = response.data.dataForSOH;
			
		});
		
		$http.get("json/sales_overview.json").then(function(response){
			$scope.b2bStoresData = response.data.dataForB2B;
			$scope.clustersData = response.data.dataForClusters;
			$scope.lolStoresData = response.data.dataForLOL;
			$scope.spsfStoresData = response.data.dataForSPSF;
		})
		$http.get("json/store_overview.json").then(function(response){
			$scope.storesData = response.data[0];
			$scope.storesDataForLOL = response.data[1];
			$scope.storesDataForSTD = response.data[2];
			$scope.storesDataForYTD = response.data[3];
		})
		
		$http.get("json/discounts_overview.json").then(function(response){
			$scope.discountsOverviewStoreclusters = response.data[0];
			$scope.discountsOverviewMarchandiseclusters = response.data[1];
		})
//		
//		$http.get('/sku/saveData').then(function(response){
//			console.log(response);
//		})
		$http.get("json/sample_retail.json").then(function(data){
			$scope.jsonData = data.data;
		})
		
		
		$scope.OpenFileUploadModelBox = function(){
			var modalInstance = $uibModal.open({
	            templateUrl: '/resources/pages/retail/fileUpload.html',
	            controller: 'FileUploadCtrl',
	            backdrop: false,
	            scope: $scope
	            
	        });
			modalInstance.result.then(function() {
	        	console.log('Clicked on Save');
	        }, function() {
	            console.log('Clicked on Cancel');
	        });
		}
		
			 $scope.logout = function () {
	               UserService.logout();
	           }
        }]);
inferneonApp.controller('FileUploadCtrl', ['$scope' ,'$http','$location','Upload', '$rootScope','MessageService','$timeout', '$uibModal','RetailService','MessageBus','UserService',
                                          function ($scope, $http,$location,Upload, $rootScope, MessageService, $timeout, $uibModal,RetailService,MessageBus,UserService) {
	$scope.files = [];
	$scope.setFiles = function(element) {
		console.log("coming here");
		$scope.$apply(function($scope) {
					for (var i = 0; i < element.files.length; i++) {
						$scope.files.push(element.files[i])
					}
					$scope.progressVisible = false
				});
	};
	$scope.uploadDataFile = function(index) {
		console.log(index);
		console.log($scope.files[index]);
		$scope.vm.projectReady = false;
		var inferUrl = "/sku";
		Upload.upload({
			method : "POST",
			url : inferUrl,
			file : $scope.files[index],
			headers : {
				 'Accept': 'application/json',
	     	    'Content-Type': 'application/json'
			},
		}).progress(function(evt) {
		      $scope.progressVisible = true;
		      $scope.files[index].progress = Math.round(evt.loaded * 100 / evt.total);
		}).success(function(response) {
//			 angular.element("input[type='file']").val(null);
				console.log(response);
		});
	}
	$scope.removeFromQueue = function(index) {
//		 angular.element("input[type='file']").val(null);
	     $scope.files.splice(index, 1);
	     if($scope.files.length == 0) {
	      $scope.progressVisible = false;
	     }
	    }
}]);
     	 
	
