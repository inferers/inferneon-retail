////////////////////T///////////////////////////////////////////////////////////////////////////////////////
//
// Defines the javascript files that need to be loaded and their dependencies.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////

require.config({
    paths: {
        'angular': '../bower_components/angular/angular',
        'angularMessages': '../bower_components/angular-messages/angular-messages',
        'angularResource': '../bower_components/angular-resource/angular-resource',
        'ui.bootstrap.tpls': '../bower_components/angular-bootstrap/ui-bootstrap-tpls.min',
        'bootstrap_min': '../bower_components/bootstrap/dist/js/bootstrap.min',
        'csrfInterceptor': '../bower_components/spring-security-csrf-token-interceptor/dist/spring-security-csrf-token-interceptor.min',
        'underscore': '../bower_components/underscore/underscore.min',
        'lodash': '../bower_components/lodash/lodash',
        'bootstrapTreeView': '../bower_components/bootstrap-treeview/dist/bootstrap-treeview.min',
        'ngFileUpload': '../bower_components/ng-file-upload/ng-file-upload',
        'jQuery': "../bower_components/jquery/dist/jquery.min",
        'jQueryUi':"../bower_components/jquery-ui/jquery-ui",
        'jQueryUiSlider':"../bower_components/jquery-ui/ui/slider",
        'editableTableWidgets': '../public/js/editable-table-widgets',
        'ngGrid': '../bower_components/ng-grid/ng-grid-2.0.14.min',
        'angular-ui-router' : '../bower_components/angular-ui-router/release/angular-ui-router.min',
        'frontendServices': 'frontend-services',        
        'inferneonApp': "inferneon-app",
        'commonServices' : 'common-services',
   },
    shim: {
       'jQuery': {exports: "jQuery"},
        'bootstrap_min': {deps:['jQuery']},
        'angular': {exports: "angular"},
        'csrfInterceptor': {deps: ['angular']},        
        'angularMessages': {deps: ['angular']},
        'angularResource': {deps: ['angular']},        
        'editableTableWidgets': {deps: ['angular', 'lodash']},
        'commonServices': {deps: ['angular', 'lodash', 'csrfInterceptor']},
        'frontendServices': {deps: ['commonServices', 'angularResource']},
        'ui.bootstrap.tpls': {deps: ['angular']},
        'ngFileUpload': {deps: ['angular']},
        'ngGrid': {deps: ['angular','jQuery']},       
        'bootstrapTreeView': {deps: ['jQuery']},
        'angular-ui-router' : {deps: ['angular']},
        'inferneonApp': {deps: ['jQuery','bootstrap_min','lodash', 'angular','angularMessages','angularResource','ui.bootstrap.tpls','editableTableWidgets' , 'frontendServices', 
                                'commonServices','ngFileUpload','ngGrid','bootstrapTreeView','angular-ui-router']}
    }
});

require(['inferneonApp'], function () {
 angular.bootstrap(document.getElementById('inferneonApp'), ['inferneonApp']);
});