angular.module('frontendServices', ['commonServices','ngResource'])
.service('UserService', ['$http','$q', function($http, $q) {
	  return {
		  getUserInfo: function() {
              var deferred = $q.defer();
              $http.get('/user')
                  .then(function (response) {
                      if (response.status == 200) {
                          deferred.resolve(response.data);
                      }
                      else {
                          deferred.reject('Error retrieving user info');
                      }
              });

              return deferred.promise;
          },
          logout: function () {
              $http({
                  method: 'POST',
                  url: '/logout'
              })
              .then(function (response) {
                  if (response.status == 200) {
                  window.location.reload();
                  }
                  else {
                      console.log("Logout failed!");
                  }
              });
          }
	  }
	
}]).service('RetailService', ['$http','$q', function($http, $q) {
            return {
           	 loadCSVData: function(type,searchByName,groupByName) {
                    var deferred = $q.defer();
                    $http.get('/retail/visualize',{
                    	 params: {
                    		 type :type,
                    		 typeValue: groupByName,
                    		 groupBy: searchByName,
             			  }
                    })
                   .then(function (response) {
                       if (response.status == 200) {
                           deferred.resolve(response.data);
                       }
                       else {
                           deferred.reject('Error retrieving list of files');
                       }
                   });

                   return deferred.promise;
               },
            loadSKUData: function() {
                   var deferred = $q.defer();
                   $http.get('/retail/loadAll')
                  .then(function (response) {
                      if (response.status == 200) {
                          deferred.resolve(response.data);
                      }
                      else {
                          deferred.reject('Error retrieving list of files');
                      }
                  });

                  return deferred.promise;
              },
              loadSKUByChannelAndStore :function(channelName,storeName){
            	  var deferred = $q.defer();
            	  $http({
            		  method :"GET",
            		  url :'/retail/loadSKUByChannelAndStore',
            		  params :{
            			  		channelName :channelName,
            			  		storeName :storeName
            			 	  }
            	  	})
            	   .then(function(response){
            		  if(response.status == 200){
            			  deferred.resolve(response.data)
            		  }else{
            			  deferred.reject('Error while retriving the SKUs')
            		  }
            	  });
            	  return deferred.promise;
              },
              loadAllStores :function(){
            	  var deferred = $q.defer();
            	  $http.get('/retail/loadAllChannelsWithStores')
            	  .then(function(response){
            		  if(response.status == 200){
            			  deferred.resolve(response.data);
            		  }else{
            			  deferred.reject("Error while retriving the store details");
            		  }
            	  })
            	  return deferred.promise;
              },
              saveSubstituteSku :function(postData){
            	  var deferred = $q.defer();
            	  $http({
            		  method :"POST",
            		  url :"/retail/saveSubstituteSku",
            		  data : postData,
            		  headers :{
            			  'Accept':'application/json',
            			  'content-type':'application/json'
            		  }
            	  }).then(function(response){
            		  if(response.status == 200){
            			  deferred.resolve(response.data);
            		  }else{
            			  deferred.reject("Error while saving the substitutedsku")
            		  }
            	  })
            	  return deferred.promise;
              },
              saveEditedSkusAndFeautersData :function(postData){
            	  var deferred = $q.defer();
            	  $http({
            		  method :"POST",
            		  url :"/retail/saveEditedSkusAndFeauters",
            		  data : postData,
            		  headers :{
            			  'Accept':"application/json",
            			  'Content-type':"application/json"
            		  }
            	  }).then(function(response){
            		  if(response.status == 200){
            			  deferred.resolve(response.data);
            		  }else{
            			  deferred.reject("Error while saving Edited Skus and its Feauters");
            		  }
            	  })
            	  return deferred.promise;
              }
           }
       }]);