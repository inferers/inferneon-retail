package com.ipsg.inferneon.app.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.ipsg.inferneon.app.model.SKU;

@Repository
public class SKURepository {
	
	private static final Logger LOG = Logger.getLogger(SKURepository.class);
	@PersistenceContext
	EntityManager em;

	public void saveSKUData(SKU sku,int rowCount) {
		em.persist(sku);
		if(rowCount%50 == 0){
			LOG.info("cleared sku resources from session");
			em.flush();
			em.clear();
		}
	}
		
	}
