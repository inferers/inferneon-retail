package com.ipsg.inferneon.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="SKU_ATTRIBUTES")
public class SKUAttributes extends AbstractEntity {
	
	@Column(name="Style")
	private String style;
	
	@Column(name="Colour")
	private String color;
	
	@Column(name="Size")
	private String size;
	
	@OneToOne
	@JsonBackReference("skuattrs-sku")
	private SKU sku;
	
	public SKUAttributes() {
		super();
	}
	public SKUAttributes(String style, String color, String size) {
		super();
		this.style = style;
		this.color = color;
		this.size = size;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public SKU getSku() {
		return sku;
	}
	public void setSku(SKU sku) {
		this.sku = sku;
	}
	
	
	

}
