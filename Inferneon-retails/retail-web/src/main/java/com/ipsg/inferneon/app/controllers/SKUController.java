package com.ipsg.inferneon.app.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ipsg.inferneon.app.services.SKUService;

@Controller
@RequestMapping("sku")
public class SKUController {
	
	@Autowired
	SKUService skuService;
	
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST,consumes = { "multipart/form-data" })
	public void saveFileDataIntoDatabase(Principal principal, MultipartHttpServletRequest request) throws IOException, InvalidFormatException{
		System.out.println("coming here"+principal.getName());
		MultipartFile multiFile = request.getFile(request.getFileNames().next());
		System.out.println("fileName is ::::::::::::: " +multiFile.getOriginalFilename());
		InputStream is = multiFile.getInputStream();
//		File excelFilePath = new File("/home/ipsg/workspace/retails/sales_last3years.xlsx");
		Workbook workbook = new SXSSFWorkbook();
		workbook = WorkbookFactory.create(is); 
		Sheet firstSheet = workbook.getSheetAt(0);
//		Sheet secondSheet = workbook.getSheetAt(1);
		Iterator<Row> iterator1 = firstSheet.iterator();
//		Iterator<Row> secondSheetData = secondSheet.iterator();
		skuService.saveExcelSheetDataIntoDatabase(iterator1);
	}		
}