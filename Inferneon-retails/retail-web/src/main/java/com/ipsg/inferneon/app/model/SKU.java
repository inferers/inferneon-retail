package com.ipsg.inferneon.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="SKU")
public class SKU extends AbstractEntity {
	
	@Column(name="SKU")
	private String sku;

	@OneToOne(cascade = CascadeType.ALL)
	@JsonManagedReference("skuattrs-sku")
	private SKUAttributes skuAttributes;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JsonManagedReference("collection-sku")
	private Collection collection;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JsonManagedReference("product-sku")
	private Product product;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JsonManagedReference("rates-sku")
	private Rates rates;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JsonManagedReference("sku-time")
	private TimePeriod timePeriod;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "sku_store", catalog = "inferneon_retail", joinColumns = { @JoinColumn(name = "sku", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "stores", nullable = false, updatable = false) })
	private Set<Stores> stores = new HashSet<Stores>(0);
	
	public SKU(String sku) {
		this.sku = sku;
	}

	public SKU() {
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public SKUAttributes getSkuAttributes() {
		return skuAttributes;
	}

	public void setSkuAttributes(SKUAttributes skuAttributes) {
		this.skuAttributes = skuAttributes;
	}

	public Collection getCollection() {
		return collection;
	}

	public void setCollection(Collection collection) {
		this.collection = collection;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}


	public Rates getRates() {
		return rates;
	}

	public void setRates(Rates rates) {
		this.rates = rates;
	}

	public Set<Stores> getStores() {
		return stores;
	}

	public void setStores(Set<Stores> stores) {
		this.stores = stores;
	}

	public TimePeriod getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(TimePeriod timePeriod) {
		this.timePeriod = timePeriod;
	}
	
	

}
