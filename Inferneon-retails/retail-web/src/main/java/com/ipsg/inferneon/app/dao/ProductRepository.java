package com.ipsg.inferneon.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.ipsg.inferneon.app.model.Collection;
import com.ipsg.inferneon.app.model.Product;
import com.ipsg.inferneon.app.model.Stores;

@Repository
public class ProductRepository {

	Logger logger = Logger.getLogger(ProductRepository.class);
	
	@PersistenceContext
	EntityManager em;
	
	public Product loadProductByProductName(String productName) {
		 
		Product product = em.createNamedQuery(Product.LoadProductByName,Product.class)
				.setParameter("productName", productName)
				.getSingleResult();
		
		return product;
	}

	public boolean isProductAvailable(String productName) {
			List<Product> product = em.createNamedQuery(Product.LoadProductByName,Product.class)
					.setParameter("productName", productName)
					.getResultList();
			return product.isEmpty();
		}

	public boolean isColectionAvailable(String collectionName) {
		List<Collection> collection = em.createNamedQuery(Collection.LoadCollectionByName,Collection.class)
				.setParameter("collectionType", collectionName)
				.getResultList();
		return collection.isEmpty();
	}

	public Collection loadCollectionByName(String collectionName) {
		Collection collection = em.createNamedQuery(Collection.LoadCollectionByName,Collection.class)
				.setParameter("collectionType", collectionName)
				.getSingleResult();
		return collection;
	}

	public boolean isStoreAvailable(String storeName) {
		List<Stores> stores = em.createNamedQuery(Stores.LoadStoreByName,Stores.class)
				.setParameter("storeName", storeName)
				.getResultList();
		return stores.isEmpty();
	}

	public Stores loadStoreByStoreName(String storeName) {
		Stores store = em.createNamedQuery(Stores.LoadStoreByName,Stores.class)
						.setParameter("storeName", storeName)
						.getSingleResult();
		return store;
	}
	}
