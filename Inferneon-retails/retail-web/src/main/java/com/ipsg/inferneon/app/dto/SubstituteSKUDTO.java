package com.ipsg.inferneon.app.dto;

public class SubstituteSKUDTO {
	public String skuName;
	public String substituteSkuName;
	
	public SubstituteSKUDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getSkuName() {
		return skuName;
	}
	
	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}
	public String getSubstituteSkuName() {
		return substituteSkuName;
	}
	public void setSubstituteSkuName(String substituteSkuName) {
		this.substituteSkuName = substituteSkuName;
	}

	@Override
	public String toString() {
		return "SubstituteSKUDTO [skuName=" + skuName + ", substituteSkuName="
				+ substituteSkuName + "]";
	}
	

}
