package com.ipsg.inferneon.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="PRODUCT")
@NamedQueries({
		@NamedQuery(
			name = Product.LoadProductByName,
			query = "select pr from Product pr where pr.product = :productName"
				)
		})
public class Product extends AbstractEntity {
	
	public static final String LoadProductByName = "LoadProductByName";
	@Column(name="Product_type")
	private String product;
	
	public Product(String product) {
		this.product = product;
	}

	public Product() {
		super();
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

}
