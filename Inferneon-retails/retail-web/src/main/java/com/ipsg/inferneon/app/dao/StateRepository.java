package com.ipsg.inferneon.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.ipsg.inferneon.app.model.States;

@Repository
public class StateRepository {

	Logger Log = Logger.getLogger(StateRepository.class);
	
	@PersistenceContext
	EntityManager em;

	public boolean isStateAvailable(String cityName) {
		List<States> states= em.createNamedQuery(States.LoadStatesByStateName,States.class)
				.setParameter("stateName", cityName)
				.getResultList();
		return states.isEmpty();
	}

	public States loadStateByStateName(String cityName) {
		States states = em.createNamedQuery(States.LoadStatesByStateName,States.class)
				.setParameter("stateName", cityName)
				.getSingleResult();
		return states;
	}

	public void saveState(States state,int rowCount) {
		em.persist(state);
	}
	
}
