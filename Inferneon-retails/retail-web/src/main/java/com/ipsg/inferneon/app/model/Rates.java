package com.ipsg.inferneon.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="COST_DETAILS")
public class Rates extends AbstractEntity {
	
	@Column(name="Mrp")
	private double mrp;

	@Column(name="Sales_qty")
	private int salesQty;
	
	@Column(name="Gross_sales")
	private double grossSales;
	
	@Column(name="Net_sales")
	private double netSales;
	
	@Column(name="Discount")
	private double discount;

	@OneToOne
	@JsonBackReference("rates-sku")
	private SKU sku;
	public Rates() {

	}

	public Rates(double mrp, int salesQty, double grossSales, double netSales,
			double discount) {
		super();
		this.mrp = mrp;
		this.salesQty = salesQty;
		this.grossSales = grossSales;
		this.netSales = netSales;
		this.discount = discount;
	}

	public double getMrp() {
		return mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public int getSalesQty() {
		return salesQty;
	}

	public void setSalesQty(int salesQty) {
		this.salesQty = salesQty;
	}

	public double getGrossSales() {
		return grossSales;
	}

	public void setGrossSales(double grossSales) {
		this.grossSales = grossSales;
	}

	public double getNetSales() {
		return netSales;
	}

	public void setNetSales(double netSales) {
		this.netSales = netSales;
	}

	public SKU getSku() {
		return sku;
	}

	public void setSku(SKU sku) {
		this.sku = sku;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	
}
