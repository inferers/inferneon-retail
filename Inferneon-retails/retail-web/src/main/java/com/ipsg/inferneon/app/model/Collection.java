package com.ipsg.inferneon.app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="COLLECTION")
@NamedQueries({
	@NamedQuery(
			name=Collection.LoadCollectionByName,
			query = "select cl from Collection cl where cl.collectionType =:collectionType")	
})
public class Collection extends AbstractEntity {
	
	public static final String LoadCollectionByName = "LoadCollectionByName";
	
	@Column(name="Collection")
	private String collectionType;
	
	@OneToMany
	@JsonBackReference("collection-sku")
	private Set<SKU> sku;

	public Collection() {
		super();
	}

	public Collection(String collectionType) {
		super();
		this.collectionType = collectionType;
	}

	public String getCollectionType() {
		return collectionType;
	}

	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}

	public Set<SKU> getSku() {
		return sku;
	}

	public void setSku(Set<SKU> sku) {
		this.sku = sku;
	}
	
}
