package com.ipsg.inferneon.app.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="STATES")
@NamedQueries({
	@NamedQuery(name=States.LoadStatesByStateName,
			query = "select st from States st where st.state =:stateName"),
			@NamedQuery(name=States.LoadCityByCityName,
						query = "select st from States st where st.city = :cityName")
})
public class States extends AbstractEntity {
	public static final String LoadStatesByStateName = "LoadStatesByStateName";
	public static final String LoadCityByCityName = "LoadCityByCityName";
	@Column(name="State_name")
	private String state;
	
	@Column(name="Zone")
	private String zone;
	
	@Column(name="City")
	private String city;
	
	@OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JsonManagedReference("state-store")
	private Set<Stores> stores;
	
	public States() {
		super();
	}

	public States(String state, String zone) {
		super();
		this.state = state;
		this.zone = zone;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Set<Stores> getStores() {
		return stores;
	}

	public void setStores(Set<Stores> stores) {
		this.stores = stores;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}


}
