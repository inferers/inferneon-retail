package com.ipsg.inferneon.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="TIME_PERIOD")
public class TimePeriod extends AbstractEntity {
	
	@Column(name="Year")
	private String year;
	
	@Column(name="Calender_day")
	private String day;
	
	@Column(name="Week")
	private String week;
	
	@OneToOne
	@JsonBackReference("sku-time")
	private SKU sku;

	
	
	public TimePeriod() {
		super();
	}

	public TimePeriod(String year, String day, String week) {
		super();
		this.year = year;
		this.day = day;
		this.week = week;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public SKU getSku() {
		return sku;
	}

	public void setSku(SKU sku) {
		this.sku = sku;
	}
}
