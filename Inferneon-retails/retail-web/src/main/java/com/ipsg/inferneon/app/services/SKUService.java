package com.ipsg.inferneon.app.services;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.collections.IteratorUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipsg.inferneon.app.dao.ProductRepository;
import com.ipsg.inferneon.app.dao.SKURepository;
import com.ipsg.inferneon.app.dao.StateRepository;
import com.ipsg.inferneon.app.model.Collection;
import com.ipsg.inferneon.app.model.DBValues;
import com.ipsg.inferneon.app.model.Product;
import com.ipsg.inferneon.app.model.Rates;
import com.ipsg.inferneon.app.model.SKU;
import com.ipsg.inferneon.app.model.SKUAttributes;
import com.ipsg.inferneon.app.model.States;
import com.ipsg.inferneon.app.model.Stores;
import com.ipsg.inferneon.app.model.TimePeriod;

@Service
public class SKUService {
	@Autowired
	SKURepository skuRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	StateRepository stateRepository;

	@Transactional
	public void saveExcelSheetDataIntoDatabase(Iterator<Row> iterator) {
		int i =0;
		while (iterator.hasNext()) {
			System.out.println(i);
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			List<Cell> arrayListCells = IteratorUtils.toList(cellIterator);
			Product product;
			States states;
			Collection collection ;
			Stores store;
			if(i!=0){
				DBValues values = new DBValues();
				SKU sku = new SKU(arrayListCells.get(8).getStringCellValue());
				Set<SKU> skus = new HashSet<SKU>();
				skus.add(sku);
				if(!productRepository.isProductAvailable(arrayListCells.get(12).getStringCellValue())){
					product = productRepository.loadProductByProductName(arrayListCells.get(12).getStringCellValue());
				}else{
					product = new Product(arrayListCells.get(12).toString());
				}
				sku.setProduct(product);
				
				TimePeriod timePeriod = new TimePeriod(arrayListCells.get(1).toString(),arrayListCells.get(2).toString(),arrayListCells.get(3).toString());
				timePeriod.setSku(sku);
				sku.setTimePeriod(timePeriod);
				
				if(!productRepository.isStoreAvailable(arrayListCells.get(7).getStringCellValue())){
					store = productRepository.loadStoreByStoreName(arrayListCells.get(7).getStringCellValue());
				}else{
					store = new Stores(arrayListCells.get(7).getStringCellValue());
					values.setStoreName(arrayListCells.get(7).getStringCellValue());
				}
				Set<Stores> stores = new HashSet<Stores>();
				stores.add(store);
				
				sku.setStores(stores);
				store.setSku(skus);
				
				
				if(!stateRepository.isStateAvailable(arrayListCells.get(5).getStringCellValue())){
					states = stateRepository.loadStateByStateName(arrayListCells.get(5).getStringCellValue());
				}else{
					states = new States(arrayListCells.get(5).getStringCellValue(),arrayListCells.get(15).getStringCellValue());
				}
				store.setStates(states);
				stateRepository.saveState(states,i);
				
				SKUAttributes skuAttributes = new SKUAttributes(arrayListCells.get(9).toString(),arrayListCells.get(10).toString(),arrayListCells.get(11).toString());
				skuAttributes.setSku(sku);
				sku.setSkuAttributes(skuAttributes);
				
				double mrp = arrayListCells.get(13).getNumericCellValue();
				int salesQty =(int)arrayListCells.get(16).getNumericCellValue();
				double grossSales = arrayListCells.get(17).getNumericCellValue();
				double discount = arrayListCells.get(18).getNumericCellValue();
				double netSales = arrayListCells.get(19).getNumericCellValue();
				Rates cost = new Rates(mrp,salesQty,grossSales,netSales,discount);
				cost.setSku(sku);
				sku.setRates(cost);
				
				if(!productRepository.isColectionAvailable(arrayListCells.get(14).getStringCellValue())){
					collection = productRepository.loadCollectionByName(arrayListCells.get(14).getStringCellValue());
				}else{
					collection = new Collection(arrayListCells.get(14).getStringCellValue());
				}
				sku.setCollection(collection);
				skuRepository.saveSKUData(sku,i);
			}else{
				System.out.println("it has headers");
			}
			 i++;
		}
	}
	

}
