package com.ipsg.inferneon.app.retails;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import au.com.bytecode.opencsv.CSVReader;


public class DataViewer {

	public JSONObject filterData(String filterType, String filterName, String groupby) {

		JSONObject data = new JSONObject();
		JSONArray product = new JSONArray();
		HashMap<String, Integer> mapOfStoreData = new HashMap<String, Integer>();
		try {
			FileInputStream fr = new FileInputStream(
					"/home/ipsg/workspace/inferneon_code/code-master/inferneon/inferneon-resources/src/main/resources/TestResources/SalesData/Ril_sale.csv");
			InputStreamReader inputStream = new InputStreamReader(fr);

			FileInputStream fr2 = new FileInputStream(
					"/home/ipsg/workspace/inferneon_code/code-master/inferneon/inferneon-resources/src/main/resources/TestResources/SalesData/Ril_master.csv");
			InputStreamReader inputStream2 = new InputStreamReader(fr2);

			CSVReader csvReader = new CSVReader(inputStream);
			List<String[]> rowsList = csvReader.readAll();
			csvReader.close();
			rowsList.remove(0);

			CSVReader csvReader2 = new CSVReader(inputStream2);
			List<String[]> rowsList2 = csvReader2.readAll();
			csvReader2.close();
			rowsList2.remove(0);

			for (String[] rows : rowsList) {

				String filterNamefromCSV = null;
				if (StringUtils.equalsIgnoreCase(filterType.trim(), "Product")) {
					filterNamefromCSV = rows[12].trim();
				} else if (StringUtils.equalsIgnoreCase(filterType.trim(), "Collection")) {
					filterNamefromCSV = rows[14].trim();
				} else if (StringUtils.equalsIgnoreCase(filterType.trim(), "Store")) {
					filterNamefromCSV = rows[7].trim();
				}

				String groupValue = null;
				if (StringUtils.equalsIgnoreCase(groupby.trim(), "Product")) {
					groupValue = rows[12].trim();
				} else if (StringUtils.equalsIgnoreCase(groupby.trim(), "Collection")) {
					groupValue = rows[14].trim();
				} else if (StringUtils.equalsIgnoreCase(groupby.trim(), "Store")) {
					groupValue = rows[7].trim();
				}else if (StringUtils.equalsIgnoreCase(groupby.trim(), "gender")) {
					String eanNumber = rows[8].trim();
					for(String[] rows2 : rowsList2){
						if(StringUtils.equalsIgnoreCase(eanNumber, rows2[0])){
							groupValue = rows2[8];
							break;
						}
					}
				}
				if (StringUtils.equalsIgnoreCase(filterNamefromCSV, filterName)) {

					if (mapOfStoreData.containsKey(groupValue)) {
						Integer count = mapOfStoreData.get(groupValue);
						mapOfStoreData.put(groupValue, ++count);
					} else {
						mapOfStoreData.put(groupValue, 1);
					}
				}
			}

			for (Map.Entry<String, Integer> entry : mapOfStoreData.entrySet()) {
				JSONObject dataObj = new JSONObject();
				dataObj.put("label", entry.getKey());
				dataObj.put("value", entry.getValue());
				product.add(dataObj);
			}
			data.put("data", product);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(data.toString());
		return data;
	}

	public static void main(String arg[]) {
		DataViewer ab = new DataViewer();
		ab.filterData("store", "BSR Complex Bglr", "gender");
	}

}