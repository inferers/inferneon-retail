package com.ipsg.inferneon.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="STORES")
@NamedQueries({
	@NamedQuery(name=Stores.LoadStoreByName,
			query="select store from Stores store where store.storeName =:storeName")
})
public class Stores extends AbstractEntity{
	public static final String LoadStoreByName = "LoadStoreByName";
	
	@Column(name="Store_name")
	private String storeName;
	
	@ManyToOne
	@JsonBackReference("state-store")
	private States states;

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "sku")
	@JsonIgnore
	private Set<SKU> sku = new HashSet<SKU>(0);
	
	
	public Stores() {
		super();
	}

	public Stores(String storeName) {
		super();
		this.storeName = storeName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public States getStates() {
		return states;
	}

	public void setStates(States states) {
		this.states = states;
	}

	public Set<SKU> getSku() {
		return sku;
	}

	public void setSku(Set<SKU> sku) {
		this.sku = sku;
	}
	

}
