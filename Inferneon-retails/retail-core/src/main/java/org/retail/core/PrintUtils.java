package org.retail.core;

import java.util.stream.DoubleStream;

public class PrintUtils {
	public static void printDoubleArray(double[] doubleArray){
		System.out.print("{ ");
		for(int arrayIndex = 0; arrayIndex < doubleArray.length;arrayIndex++){
			if(arrayIndex != doubleArray.length-1){
				System.out.print(doubleArray[arrayIndex] +  ", ");
			}
			else{
				System.out.print(doubleArray[arrayIndex]);
			}
		}
		System.out.println("}");
	}
}
