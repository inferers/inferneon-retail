package org.retail.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.DoubleStream;

import edu.stanford.nlp.optimization.DiffFunction;

public class GenericMLFunction implements DiffFunction {

	// TODO KEEP IN MIND TO GIVE VALUE TO X ACCORDING TO THE LIST OF SKU THAT WE
	// GET FROM skuVsAttrTypeMapping.keySet().
	public double[] x;
	public List<String> attrTypeNumMapping = new ArrayList<String>();
	public Map<String, List<String>> attrVsAttrTypesMap = new LinkedHashMap<String, List<String>>();
	public Map<String, List<List<String>>> skuVsSubstitutableSKUAttrsListMap = new LinkedHashMap<String, List<List<String>>>();;
	public Map<String, List<String>> skuVsAttrTypeMapping = new LinkedHashMap<String, List<String>>();
	public Map<String, List<Integer>> skuVsSubstitionIndexsMap = new HashMap<String, List<Integer>>();
	List<String> substitutableList = new ArrayList<String>();
	List<String> substituteList = new ArrayList<String>();
	private int derivateWithRespectTo;
	private int numDimensions;

	protected void updateSKUVsSubstitutionIndexListMap() {
		for (Map.Entry<String, List<String>> skuVsAttrTypeList : skuVsAttrTypeMapping.entrySet()) {
			for (int index = 0; index < skuVsAttrTypeList.getValue().size(); index++) {
				if (substituteList.contains(skuVsAttrTypeList.getValue().get(index))) {
					updateSKUVsSubstitutableMapForSKU(skuVsAttrTypeList.getKey(), skuVsAttrTypeList.getValue());
					break;
				}
			}
		}
	}

	private void updateSKUVsSubstitutableMapForSKU(String sku, List<String> skuAttrList) {
		List<Integer> substitutionIndexList = new ArrayList<Integer>();
		for(int substitutionIndex =0; substitutionIndex < substituteList.size(); substitutionIndex++ ){
			if(skuAttrList.contains(substituteList.get(substitutionIndex))){
				substitutionIndexList.add(substitutionIndex);
				skuVsSubstitionIndexsMap.put(sku, substitutionIndexList);
			}
		}

		int totalSubstituteAttr = substitutionIndexList.size();
		for(int indexOfSubstitutionIndexList = 0; indexOfSubstitutionIndexList < totalSubstituteAttr;indexOfSubstitutionIndexList++){
			List<Boolean> substitutionCombination = new ArrayList<Boolean>();
			for(int index=0;index < totalSubstituteAttr; index++){
				substitutionCombination.add(false);
				if(index == indexOfSubstitutionIndexList){
					substitutionCombination.set(index, false);
				}
			}
			substitutionCombination.set(indexOfSubstitutionIndexList, true);
			updateSKUVsSubstitutableSKUAttrListMapforSKU(sku, substitutionIndexList, substitutionCombination, indexOfSubstitutionIndexList);
		}
	}

	private void updateSKUVsSubstitutableSKUAttrListMapforSKU(String sku, List<Integer> substitutionIndexList, List<Boolean> substitutionCombination,int indexOfSubstitutionIndexList) {


		for(; indexOfSubstitutionIndexList < substitutionIndexList.size(); indexOfSubstitutionIndexList++){
			substitutionCombination.set(indexOfSubstitutionIndexList, true);
			List<String>  substitutableSKUAttrs = new ArrayList<String>(attrVsAttrTypesMap.keySet().size());
			List<String> substituteSKUAttrs = skuVsAttrTypeMapping.get(sku);
			for(int attrIndex = 0; attrIndex < substituteSKUAttrs.size(); attrIndex++){	
				boolean add = true;
				for(int combinationIndex = 0; combinationIndex < substitutionCombination.size(); combinationIndex++){
					if(substitutionCombination.get(combinationIndex)){
						int substitutionIndex = substitutionIndexList.get(combinationIndex);
						if(substituteList.get(substitutionIndex).equalsIgnoreCase(substituteSKUAttrs.get(attrIndex))){
							substitutableSKUAttrs.add(substitutableList.get(substitutionIndex));
							add = false;
						}
					}
				}
				if(add){
					substitutableSKUAttrs.add(substituteSKUAttrs.get(attrIndex));
				}
			}
			if(!checkSubstitutableSKUAttrInAssortment(substitutableSKUAttrs)){
				List<List<String>> substitutableSKUAttrsList = new ArrayList<List<String>>();
				if(skuVsSubstitutableSKUAttrsListMap.containsKey(sku)){
					substitutableSKUAttrsList =	skuVsSubstitutableSKUAttrsListMap.get(sku);
				}
				substitutableSKUAttrsList.add(substitutableSKUAttrs);
				skuVsSubstitutableSKUAttrsListMap.put(sku, substitutableSKUAttrsList);
			}

		}
	}

	private boolean checkSubstitutableSKUAttrInAssortment(List<String> substitutableSKUAttrList) {
		for (Map.Entry<String, List<String>> skuVsAttrTypeList : skuVsAttrTypeMapping.entrySet()) {
			if(skuVsAttrTypeList.getValue().equals(substitutableSKUAttrList)){
				return true;
			}
		}
		return false;
	}

	@Override
	public double[] derivativeAt(double point[]) {
		int totalAttrTypeSize = attrTypeNumMapping.size();

		double[] derivatives = new double[point.length];
		for (derivateWithRespectTo = 0; derivateWithRespectTo < derivatives.length; derivateWithRespectTo++) {
			if (derivateWithRespectTo < totalAttrTypeSize) {
				derivatives[derivateWithRespectTo] = derivativeFunctionForAttrTypes(derivateWithRespectTo, point);
			} else {
				derivatives[derivateWithRespectTo] = derivateFunctionForSubstitutions(derivateWithRespectTo, point);
			}
		}
		System.out.println("Value of derivatives is : ");
		PrintUtils.printDoubleArray(derivatives);
		return derivatives;
	}

	private double derivativeFunctionForAttrTypes(int derivateWithRespectTo, double[] point) {
		double sum = 0.0;

		sum -= getDerivativeValueOfLogL(derivateWithRespectTo, point);

		sum += getDerivativeValueOfLogR(derivateWithRespectTo, point);

		return sum;
	}

	private double getDerivativeValueOfLogL(int derivateWithRespectTo, double[] point) {
		double sum = 0.0;
		List<String> skus = new ArrayList<String>(skuVsAttrTypeMapping.keySet());
		for (int skuIndex = 0; skuIndex < skus.size(); skuIndex++) {
			List<String> skuAttrSet = skuVsAttrTypeMapping.get(skus.get(skuIndex));
			String sku = skus.get(skuIndex);
			if(skuVsSubstitutableSKUAttrsListMap.keySet().contains(sku)){
				if (skuAttrSet.contains(attrTypeNumMapping.get(derivateWithRespectTo))) {
					sum += (x[skuIndex] * (derFunLogLIfContainsAttrType(derivateWithRespectTo, true, point, sku, null) + substituteFunction(sku,  point, derivateWithRespectTo)))/getValueFractionOfSKUWithSubstitution(sku, point);
				} else {
					sum += (x[skuIndex] * (derFunLogLIfContainsAttrType(derivateWithRespectTo, false, point, sku, null) + substituteFunction(sku, point, derivateWithRespectTo)))/getValueFractionOfSKUWithSubstitution(sku, point);
				}
			}
			else{
				if (skuAttrSet.contains(attrTypeNumMapping.get(derivateWithRespectTo))) {
					sum += (x[skuIndex] * (derFunLogLIfContainsAttrType(derivateWithRespectTo, true, point, sku, null)))/getValueFractionOfSKUWithSubstitution(sku, point);
				} else {
					sum += (x[skuIndex] * (derFunLogLIfContainsAttrType(derivateWithRespectTo, false, point, sku, null)))/getValueFractionOfSKUWithSubstitution(sku, point);
				}
			}
		}
		return sum;
	}

	private double derFunLogLIfContainsAttrType(Integer deriveWithRespectTo, boolean containsAttrType, double[] point, String sku, List<String> substitutableSKUAttrs) {
		double numeratorSum = 0.0;
		double denominatorSum = 0.0;
		String attrTypeToDerivateWithRespectTo = attrTypeNumMapping.get(deriveWithRespectTo);
		for (Entry<String, List<String>> attrVsAttrTypesForAttr : attrVsAttrTypesMap.entrySet()) {
			List<String> attrTypesOfAttr = attrVsAttrTypesForAttr.getValue();
			if (attrVsAttrTypesForAttr.getValue().contains(attrTypeToDerivateWithRespectTo)) {
				for (int attrTypeIndex = 0; attrTypeIndex < attrTypesOfAttr.size(); attrTypeIndex++) {
					String attrType = attrTypesOfAttr.get(attrTypeIndex);
					if (containsAttrType) {
						if (!attrType.equalsIgnoreCase(attrTypeToDerivateWithRespectTo)) {
							numeratorSum += Math.exp(point[attrTypeNumMapping.indexOf(attrTypesOfAttr.get(attrTypeIndex))]);
						}
					} else {
						if (attrType.equalsIgnoreCase(attrTypeToDerivateWithRespectTo)) {
							numeratorSum = -Math.exp(point[deriveWithRespectTo]);
						}
					}
					denominatorSum += Math.exp(point[attrTypeIndex]);

				}

				break;
			}

		}
		if(substitutableSKUAttrs == null){
			double result = ((numeratorSum * valueFractionOfSKU(sku, point))/ denominatorSum) ;
			return result;
		}
		else{
			return ((numeratorSum * valueFractionOfSKUAttrTypes(substitutableSKUAttrs, point))/ denominatorSum) ;
		}
	}

	private double getDerivativeValueOfLogR(int derivateWithRespectTo, double[] point) {
		return DoubleStream.of(x).sum() * getDerivativeValueOfFractionOfAssortment(derivateWithRespectTo, point)
				/ valueAt(point, false);
	}

	private double getDerivativeValueOfFractionOfAssortment(int derivateWithRespectTo, double[] point) {
		double sum = 0.0;
		List<String> skuList = new ArrayList<String>(skuVsAttrTypeMapping.keySet());
		for (int skuIndex = 0; skuIndex < skuList.size(); skuIndex++) {
			String sku = skuList.get(skuIndex);
			List<String> skuAttrSet = skuVsAttrTypeMapping.get(sku);
			if(skuVsSubstitionIndexsMap.keySet().contains(sku)){
				if (skuAttrSet.contains(attrTypeNumMapping.get(derivateWithRespectTo))) {
					sum += derFunLogLIfContainsAttrType(derivateWithRespectTo, true, point, sku, null) + substituteFunction(sku, point, derivateWithRespectTo);
				} else {
					sum += derFunLogLIfContainsAttrType(derivateWithRespectTo, false, point, sku, null) + substituteFunction(sku,  point, derivateWithRespectTo);
				}
			}else{
				if (skuAttrSet.contains(attrTypeNumMapping.get(derivateWithRespectTo))) {
					sum += derFunLogLIfContainsAttrType(derivateWithRespectTo, true, point, sku, null);
				} else {
					sum += derFunLogLIfContainsAttrType(derivateWithRespectTo, false, point, sku, null);
				}
			}
		}
		return sum;
	}

	private double derivateFunctionForSubstitutions(int derivateWithRespectTo, double[] point) {
		double sumL = 0.0;
		double sumR = 0.0;
		double sumRDenominator = 0.0;
		List<String> skuList = new ArrayList<String>(skuVsAttrTypeMapping.keySet());
		for (int skuIndex = 0; skuIndex < skuList.size(); skuIndex++) {
			String sku = skuList.get(skuIndex);
			if (skuVsSubstitutableSKUAttrsListMap.keySet().contains(sku)) {
				double temp = getDerivativeValueOfLogLForSubs(derivateWithRespectTo, sku, point);		
				sumL += x[skuIndex] * temp / getValueFractionOfSKUWithSubstitution(sku, point);
				sumR += temp;
				sumRDenominator += getValueFractionOfSKUWithSubstitution(sku, point);
			}
		}
		sumR = DoubleStream.of(x).sum() * sumR / sumRDenominator;
		return (sumR - sumL);
	}


	//the below logic is fixed here need to make it work for two logics.
	private double getDerivativeValueOfLogLForSubs(int derivateWithRespectTo, String sku, double[] point) {
		double result = 1.0;
		List<String> substituteSKUAttrList = new ArrayList<String>(skuVsAttrTypeMapping.get(sku));
		for(List<String> substitutableSKUAttrs : skuVsSubstitutableSKUAttrsListMap.get(sku)){
			for(int attrIndex = 0; attrIndex < substituteSKUAttrList.size(); attrIndex ++ ){
				if(!substituteSKUAttrList.get(attrIndex).equalsIgnoreCase(substitutableSKUAttrs.get(attrIndex))){
					for(int substitutionIndex : skuVsSubstitionIndexsMap.get(sku)){
						int substitutionParamIndex = attrTypeNumMapping.size() + substitutionIndex ;
						if(substituteSKUAttrList.get(attrIndex).equalsIgnoreCase(substituteList.get(substitutionIndex) )
								&& substitutableSKUAttrs.get(attrIndex).equalsIgnoreCase(substitutableList.get(substitutionIndex) )){
							result *=derivativeFunctionForSubstituions1(derivateWithRespectTo, substitutionParamIndex, point);
							if(substitutionParamIndex == derivateWithRespectTo){
								result *= 1 / (Math.exp(point[substitutionParamIndex]) * ( 1 + Math.exp(point[substitutionParamIndex]))); 
							}
						}
					}
				}
			}
			result *=  valueFractionOfSKUAttrTypes(substitutableSKUAttrs, point);
		}
		return result;
	}

	private double derivativeFunctionForSubstituions1(int derivateWithRespectTo, int substitutionParamIndex, double[] point) {
		double result =  Math.exp(point[substitutionParamIndex]) / (1 + Math.exp(point[substitutionParamIndex]));
		return result;
	}

	@Override
	public double valueAt(double point[]) {
		double result = valueAt(point, true);
		System.out.println("Value of Function : " +result + " at point : ");
		PrintUtils.printDoubleArray(point);
		return result;
	}

	public double valueAt(double point[], boolean logLikliHoodFun){
		//SumR = F(A)
		int count = 0;
		double sumLogL = 0.0;
		double sumR = 0.0;
		double sumX = DoubleStream.of(x).sum();


		for (Map.Entry<String, List<String>> entry : skuVsAttrTypeMapping.entrySet()) {
			double valueFractionOfSKUWithSubstitution = getValueFractionOfSKUWithSubstitution(entry.getKey(), point);
			if(logLikliHoodFun){
				sumLogL += x[count] * Math.log(valueFractionOfSKUWithSubstitution);
			}
			sumR += valueFractionOfSKUWithSubstitution;
			count++;
		}
		double sumLogR = sumX * Math.log(sumR);
		// System.out.println("Function calculation Done...");
		if(logLikliHoodFun){
			return sumLogR - sumLogL;
		}else{
			return sumR;
		}
	}

	public double getValueFractionOfSKUWithSubstitution(String sku, double[] point){
		Set<String> substituteSKUs = skuVsSubstitutableSKUAttrsListMap.keySet();
		double result = 0.0;
		if (!substituteSKUs.contains(sku)) {
			result = valueFractionOfSKU(sku, point);
		} else {
			result = valueFractionOfSKU(sku, point) + substituteFunction(sku, point, null);
		}
		return result;
	}

	private double substituteFunction(String substituteSKU, double[] point, Integer derivativeWithRespectTo) {
		double sumSubstitution = 0.0;
		if (skuVsSubstitutableSKUAttrsListMap.keySet().contains(substituteSKU)) {
			List<List<String>> substitutableSKUAttrsList = skuVsSubstitutableSKUAttrsListMap.get(substituteSKU);
			for (List<String> substitutableSKUAttrs : substitutableSKUAttrsList) {
				if(derivativeWithRespectTo == null){
					sumSubstitution += (getSubstitutionalValueForSubstituion( substituteSKU, substitutableSKUAttrs, point) * valueFractionOfSKUAttrTypes(substitutableSKUAttrs, point));
				}
				else{
					if(substitutableSKUAttrs.contains(attrTypeNumMapping.get(derivateWithRespectTo))){
						sumSubstitution += (getSubstitutionalValueForSubstituion( substituteSKU,substitutableSKUAttrs, point) * derFunLogLIfContainsAttrType(derivativeWithRespectTo, true, point, null, substitutableSKUAttrs));

					}else{
						sumSubstitution += (getSubstitutionalValueForSubstituion( substituteSKU,substitutableSKUAttrs, point) * derFunLogLIfContainsAttrType(derivativeWithRespectTo, false, point, null, substitutableSKUAttrs));
					}
				}
			}
		}
		return sumSubstitution;
	}


	//TODO possible code repetition with function getDerivativeValueOfLogLForSubs
	private double getSubstitutionalValueForSubstituion(String substituteSKU, List<String> substitutableSKUAttrs, double[] point) {
		double result = 1.0;

		List<String> substituteSKUAttrList = new ArrayList<String>(skuVsAttrTypeMapping.get(substituteSKU));
		for(int attrIndex = 0; attrIndex < substitutableSKUAttrs.size(); attrIndex ++ ){
			if(!substituteSKUAttrList.get(attrIndex).equalsIgnoreCase(substitutableSKUAttrs.get(attrIndex))){
				for(int substitutionIndex : skuVsSubstitionIndexsMap.get(substituteSKU)){
					if(substituteSKUAttrList.get(attrIndex).equalsIgnoreCase(substituteList.get(substitutionIndex) )
							&& substitutableSKUAttrs.get(attrIndex).equalsIgnoreCase(substitutableList.get(substitutionIndex) )){
						int initParamsIndex = attrTypeNumMapping.size() + substitutionIndex;
						result *= Math.exp(point[initParamsIndex])
								/ (1 + Math.exp(point[initParamsIndex]));
					}
				}
			}
		}

		return result;
	}

	protected double valueFractionOfSKU(String sku, double[] point) {
		List<String> skuAttrValues = skuVsAttrTypeMapping.get(sku);
		return valueFractionOfSKUAttrTypes(skuAttrValues, point);
	}

	private double valueFractionOfSKUAttrTypes(List<String> skuAttrValues, double[] point){
		double attrTypeProd = 1.0;
		for (String attrType : skuAttrValues) {
			double attrProb = getAttrProb(attrType, null, point);
			attrTypeProd *= attrProb;
		}
		return attrTypeProd;
	}

	private double getAttrProb(String attrType, double[] maximizedParams, double[] point) {
		double expAttr = 0.0;
		double totExpAttr = 0.0;
		for (Map.Entry<String, List<String>> attrVsAttrTypesEntry : attrVsAttrTypesMap.entrySet()) {
			if (attrVsAttrTypesEntry.getValue().contains(attrType)) {
				expAttr = getValueNumericParam(attrType, maximizedParams, point);
				totExpAttr = getSumOfAttrValues(attrVsAttrTypesEntry.getValue(), maximizedParams, point);
			}
		}
		return expAttr / totExpAttr;
	}

	private double getSumOfAttrValues(List<String> attrList, double[] maximizedParams, double[] point) {
		double sum = 0.0;
		for (int i = 0; i < attrList.size(); i++) {
			String attrValue = attrList.get(i);
			sum += getValueNumericParam(attrValue, maximizedParams, point);
		}
		return sum;
	}

	private double getValueNumericParam(String value, double[] maximizedParams, double[] point) {
		int index = 0;
		if (attrTypeNumMapping.contains(value)) {
			index = attrTypeNumMapping.indexOf(value);
		}
		if (maximizedParams == null) {
			return Math.exp(point[index]);
		} else {
			return Math.exp(maximizedParams[index]);
		}
	}

	public double[] calculateProbability(double[] maximizedParams, double[] point) {
		double[] Result;
		if(point != null){
			Result = new double[point.length];
		}
		else{
			Result = new double[maximizedParams.length];
		}
		for (int paramIndex = 0; paramIndex < Result.length; paramIndex++) {
			if (paramIndex < attrTypeNumMapping.size()) {
				Result[paramIndex] = getAttrProb(attrTypeNumMapping.get(paramIndex), maximizedParams, point);
			} else {
				if(point != null){
					Result[paramIndex] = Math.exp(point[paramIndex]) / (1 + Math.exp(point[paramIndex]));
				}
				else{
					Result[paramIndex] = Math.exp(maximizedParams[paramIndex]) / (1 + Math.exp(maximizedParams[paramIndex]));
				}
			}
		}
		return Result;

	}

	public static void main(String[] args) {
		// M -> L ; R -> G
		GenericMLFunction sample = new GenericMLFunction();
		sample.setAttrTypeNumMapping(Arrays.asList("S", "M", "L", "R", "G", "B", "Shorts", "Tees", "Polos"));
		Map<String, List<String>> attrVsTypeList = new HashMap<String, List<String>>();
		attrVsTypeList.put("Size", Arrays.asList("S", "M", "L"));
		attrVsTypeList.put("Colour", Arrays.asList("R", "G", "B"));
		attrVsTypeList.put("Type", Arrays.asList("Shorts", "Tees", "Polos"));
		sample.setAttrVsAttrTypeListMap(attrVsTypeList);
		Map<String, List<String>> skuVsAtttrTypeMapping = new LinkedHashMap<String, List<String>>();
		skuVsAtttrTypeMapping.put("3", Arrays.asList("S", "R", "Polos"));
		skuVsAtttrTypeMapping.put("4", Arrays.asList("S", "G", "Polos"));
		skuVsAtttrTypeMapping.put("5", Arrays.asList("S", "B", "Tees"));
		skuVsAtttrTypeMapping.put("7", Arrays.asList("M", "B", "Tees"));
		skuVsAtttrTypeMapping.put("8", Arrays.asList("S", "B", "Shorts"));
		skuVsAtttrTypeMapping.put("9", Arrays.asList("L", "G", "Polos"));
		sample.setSkuVsAtttrTypeMapping(skuVsAtttrTypeMapping);

		sample.substitutableList = Arrays.asList("M", "R");
		sample.substituteList = Arrays.asList("L", "G");
		sample.updateSKUVsSubstitutionIndexListMap();
		System.out.println(sample.skuVsSubstitionIndexsMap);
		System.out.println(sample.skuVsSubstitutableSKUAttrsListMap);
	}

	public List<String> getAttrTypeNumMapping() {
		return attrTypeNumMapping;
	}

	public void setAttrTypeNumMapping(List<String> attrTypeNumMapping) {
		this.attrTypeNumMapping = attrTypeNumMapping;
	}

	public Map<String, List<String>> getAttrVsAttrTypeListMap() {
		return attrVsAttrTypesMap;
	}

	public void setAttrVsAttrTypeListMap(Map<String, List<String>> attrVsAttrTypeListMap) {
		this.attrVsAttrTypesMap = attrVsAttrTypeListMap;
	}

	public Map<String, List<String>> getSkuVsAtttrTypeMapping() {
		return skuVsAttrTypeMapping;
	}

	public void setSkuVsAtttrTypeMapping(Map<String, List<String>> skuVsAtttrTypeMapping) {
		this.skuVsAttrTypeMapping = skuVsAtttrTypeMapping;
	}

	public Map<String, List<String>> getSkuVsAttrTypeMapping() {
		return skuVsAttrTypeMapping;
	}

	public void setSkuVsAttrTypeMapping(Map<String, List<String>> skuVsAttrTypeMapping) {
		this.skuVsAttrTypeMapping = skuVsAttrTypeMapping;
	}

	public List<String> getSubstitutableList() {
		return substitutableList;
	}

	public void setSubstitutableList(List<String> substitutableList) {
		this.substitutableList = substitutableList;
	}

	public List<String> getSubstituteList() {
		return substituteList;
	}

	public void setSubstituteList(List<String> substituteList) {
		this.substituteList = substituteList;
	}

	@Override
	public int domainDimension() {
		return numDimensions;
	}

	public double[] getX() {
		return x;
	}

	public void setX(double[] x) {
		this.x = x;
	}

	public void setNumDimensions(int numDimensions) {
		this.numDimensions = numDimensions;
	}

}
