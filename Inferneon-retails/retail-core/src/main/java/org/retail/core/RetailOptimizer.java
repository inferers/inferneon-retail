package org.retail.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.DoubleStream;

import edu.stanford.nlp.optimization.QNMinimizer;

public class RetailOptimizer {

	public static void main(String args[]) {
		// M -> L ; R -> G
		String csvFile = "/sampleHyderabadData.csv";
		String line = "";
		String cvsSplitBy = ",";

		try (BufferedReader br = new BufferedReader(new InputStreamReader(RetailOptimizer.class.getClass().getResourceAsStream(csvFile)))) {
			
			
			Map<String, List<String>> skuVsAtttrTypeMapping = new LinkedHashMap<String, List<String>>();
			List<String> attrTypeNumMapping = new ArrayList<String>();
			Map<String, List<String>> attrVsTypeList = new LinkedHashMap<String, List<String>>();

			// Create the two buffered readers.
			BufferedReader br1 = new BufferedReader(new InputStreamReader(RetailOptimizer.class.getClass().getResourceAsStream(csvFile)));
			int skuSize = (int) br1.lines().count() - 1;
			double[] x = new double[skuSize];
			br1.close();
			int count = 0;
			List<String> attributes = new ArrayList<String>();

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] skuAttrSalesData = line.split(cvsSplitBy);
				List<String> skuAttr = new ArrayList<String>();
				for (int dataColoumIndex = 1; dataColoumIndex < skuAttrSalesData.length - 1; dataColoumIndex++) {
					String skuAttrOrSalesData = skuAttrSalesData[dataColoumIndex];
					if (count == 0) {
						List<String> attrTypeList = new ArrayList<String>();
						attrVsTypeList.put(skuAttrOrSalesData, attrTypeList);
						attributes.add(skuAttrOrSalesData);
					} else {

						skuAttr.add(skuAttrSalesData[dataColoumIndex]);
						if (!attrTypeNumMapping.contains(skuAttrOrSalesData)) {
							attrTypeNumMapping.add(skuAttrOrSalesData);
						}
						List<String> attrTypeList = attrVsTypeList.get(attributes.get(dataColoumIndex - 1));
						if (!attrTypeList.contains(skuAttrOrSalesData)) {
							attrTypeList.add(skuAttrOrSalesData);
						}
					}
				}
				if (!(count == 0)) {
					skuVsAtttrTypeMapping.put(skuAttrSalesData[0], skuAttr);
					x[count-1] = Double.parseDouble(skuAttrSalesData[6]);
				}
				count++;
			}
			System.out.println("skuVsAtttrTypeMapping : " + skuVsAtttrTypeMapping + "\n attrTypeNumMapping : "
					+ attrTypeNumMapping + "\n attrVsTypeList : " + attrVsTypeList);

			GenericMLFunction sample = new GenericMLFunction();
			sample.setX(x);
			sample.setAttrTypeNumMapping(attrTypeNumMapping);
			// Map<String, List<String>> attrVsTypeList = new HashMap<String,
			// List<String>>();
			// attrVsTypeList.put("Size", Arrays.asList("S", "M", "L"));
			// attrVsTypeList.put("Colour", Arrays.asList("R", "G", "B"));
			// attrVsTypeList.put("Type", Arrays.asList("Shorts", "Tees",
			// "Polos"));
			sample.setAttrVsAttrTypeListMap(attrVsTypeList);
			// Map<String, List<String>> skuVsAtttrTypeMapping = new
			// LinkedHashMap<String, List<String>>();
			// skuVsAtttrTypeMapping.put("1", Arrays.asList("M", "R", "Polos"));
			// skuVsAtttrTypeMapping.put("2", Arrays.asList("M", "G", "Polos"));
			// skuVsAtttrTypeMapping.put("3", Arrays.asList("S", "R", "Polos"));
			// skuVsAtttrTypeMapping.put("4", Arrays.asList("S", "G", "Polos"));
			// skuVsAtttrTypeMapping.put("5", Arrays.asList("S", "B", "Tees"));
			// skuVsAtttrTypeMapping.put("6", Arrays.asList("L", "R", "Polos"));
			// skuVsAtttrTypeMapping.put("7", Arrays.asList("M", "B", "Tees"));
			// skuVsAtttrTypeMapping.put("8", Arrays.asList("S", "B",
			// "Shorts"));
			// skuVsAtttrTypeMapping.put("9", Arrays.asList("L", "G", "Polos"));
			sample.setSkuVsAtttrTypeMapping(skuVsAtttrTypeMapping);
			//
//			sample.substitutableList = Arrays.asList("CB/DG/WH", "L");
//			sample.substituteList = Arrays.asList("BK/AR/FG", "XL");
			sample.updateSKUVsSubstitutionIndexListMap();
			System.out.println("skuVsSubstitutableSKUsMap : " + sample.skuVsSubstitionIndexsMap);
			double[] initial = new double[attrTypeNumMapping.size() + sample.substituteList.size()];
			Random rand = new Random();
			for (int index = 0; index < initial.length; index++) {
				initial[index] = rand.nextDouble();
				System.out.println(initial[index]);
			}
			sample.setNumDimensions(initial.length);
			QNMinimizer qn = new QNMinimizer();
			double[] answer = qn.minimize(sample, 1e-10, initial);
			double totalSales = DoubleStream.of(x).sum();
			double fractionOfAssortment = 0.0 ;
			for(String sku : sample.skuVsAttrTypeMapping.keySet()){
				fractionOfAssortment +=	sample.getValueFractionOfSKUWithSubstitution(sku, answer);
			}
			double demand = totalSales / (fractionOfAssortment * 100);
			System.out.println("Answer is: " + Arrays.toString(answer));
			double[] result = sample.calculateProbability(answer, null);
			System.out.println("Result is: ");
			for (int index = 0; index < result.length; index++) {
				if (index < attrTypeNumMapping.size()) {
					System.out.println("Attribute " + attrTypeNumMapping.get(index) + " : " + result[index]);
				} else {
					System.out
							.println("Substitution " + (index - attrTypeNumMapping.size() + 1) + " : " + result[index]);
				}
			}
			System.out.println("Expected Demand For Total SKUs Possible: " + demand );
			count =0;
			double demandForThisAssortment =0.0;
			for (Map.Entry<String, List<String>> entry : sample.skuVsAttrTypeMapping.entrySet()) {
				double demandForSKU = sample.valueFractionOfSKU(entry.getKey(), answer) * 100 * demand;
				demandForThisAssortment += demandForSKU;
				double previousSales = x[count];
				System.out.println("Expected Demand for SKU (" + entry.getKey() + "): " + demandForSKU + " \nPrevious Sales for SKU (" + entry.getKey() + "): " + previousSales);
				count++;
			}
			System.out.println("Expected Demand : " + demandForThisAssortment + " \nPrevious Sales : " + totalSales);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
